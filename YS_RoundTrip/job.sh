#!/usr/bin/bash

CONFIGFILE="../circuit_scoring/src/configs/ys_round_trip_etl_fewer_events_BL1H.json" 



DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_etl_stats.csv"
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv"
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round1_etl_fewer_events_BL1H_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round1_etl_fewer_events_BL1H_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 1 ETL BL1-H complete."



DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_etl_stats.csv" 
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv" 
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round3_etl_fewer_events_BL1H_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round3_etl_fewer_events_BL1H_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 3 ETL BL1-H complete."


DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_etl_stats.csv"
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_meta.csv"
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round4_etl_fewer_events_BL1H_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round4_etl_fewer_events_BL1H_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 4 ETL BL1-H complete."

CONFIGFILE="../circuit_scoring/src/configs/ys_round_trip_rawlog10_drop_nan_bin_BL1H.json" 


DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_raw_log10_stats.csv"
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv"
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round1_rawlog10_drop_nan_BL1H_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round1_rawlog10_drop_nan_BL1H_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 1 rawlog10 BL1-H complete."



DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_raw_log10_stats.csv" 
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv" 
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round3_rawlog10_drop_nan_BL1H_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round3_rawlog10_drop_nan_BL1H_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 3 rawlog10 BL1-H complete."



DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_raw_log10_stats.csv"
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_meta.csv"
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round4_rawlog10_drop_nan_BL1H_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round4_rawlog10_drop_nan_BL1H_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 4 rawlog10 BL1-H complete."


CONFIGFILE="../circuit_scoring/src/configs/ys_round_trip_etl_fewer_events_BL1A.json" 


DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_etl_stats.csv"
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv"
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round1_etl_fewer_events_BL1A_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round1_etl_fewer_events_BL1A_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 1 ETL BL1-A complete."



DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_etl_stats.csv" 
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv" 
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round3_etl_fewer_events_BL1A_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round3_etl_fewer_events_BL1A_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 3 ETL BL1-A complete."


DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_etl_stats.csv"
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_meta.csv"
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round4_etl_fewer_events_BL1A_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round4_etl_fewer_events_BL1A_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 4 ETL BL1-A complete."


CONFIGFILE="../circuit_scoring/src/configs/ys_round_trip_rawlog10_drop_nan_bin_BL1A.json" 

DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_raw_log10_stats.csv"
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv"
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round1_rawlog10_drop_nan_BL1A_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round1_rawlog10_drop_nan_BL1A_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 1 rawlog10 BL1-A complete."


DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_raw_log10_stats.csv" 
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv" 
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round3_rawlog10_drop_nan_BL1A_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round3_rawlog10_drop_nan_BL1A_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 3 rawlog10 BL1-A complete."


DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_raw_log10_stats.csv"
METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_meta.csv"
STATSFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round4_rawlog10_drop_nan_BL1A_stats.csv"
CONDFILE="YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round4_rawlog10_drop_nan_BL1A_conditions.csv"

python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE $STATSFILE $CONDFILE

echo "Round 4 rawlog10 BL1-A complete."

