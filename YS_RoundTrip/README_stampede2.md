```bash
[bcummins@login3 ~]$  cd $STOCKYARD

[bcummins@login3 ~]$  cd GIT/sd2_data_analysis

[bcummins@login3 ~]$  idev -m 60

(base) [bcummins@c455-071 ~]$  conda activate circuit_scoring
 
(base) [bcummins@c455-071 ~]$ . YSRoundTrip_with_circuit_scoring/job.sh
```

The following is the old manual method. This information is now in `job.sh`.

%%%%%%%%%%%%%%%%%%%%%%%%%%% BL1-H %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

```bash

(circuit_scoring) [bcummins@c455-071 ~]$  CONFIGFILE="../circuit_scoring/src/configs/ys_round_trip_etl_fewer_events_BL1H.json" 



(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_etl_stats.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round1_etl_statsdf_fewer_events_BL1H.csv




(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_etl_stats.csv" 

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv" 

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round3_etl_statsdf_fewer_events_BL1H.csv




(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_etl_stats.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_meta.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round4_etl_statsdf_fewer_events_BL1H.csv




(circuit_scoring) [bcummins@c455-071 ~]$  CONFIGFILE="../circuit_scoring/src/configs/ys_round_trip_rawlog10_drop_nan_bin_BL1H.json" 



(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_raw_log10_stats.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round1_rawlog10_statsdf_drop_nan_BL1H.csv




(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_raw_log10_stats.csv" 

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv" 

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round3_rawlog10_statsdf_drop_nan_BL1H.csv




(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_raw_log10_stats.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_meta.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_H_analysis/Round4_rawlog10_statsdf_drop_nan_BL1H.csv


```


%%%%%%%%%%%%%%%%%%%%%%%%%%% BL1-A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

```bash

(circuit_scoring) [bcummins@c455-071 ~]$  CONFIGFILE="../circuit_scoring/src/configs/ys_round_trip_etl_fewer_events_BL1A.json" 



(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_etl_stats.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round1_etl_statsdf_fewer_events_BL1A.csv




(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_etl_stats.csv" 

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv" 

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round3_etl_statsdf_fewer_events_BL1A.csv




(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_etl_stats.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_meta.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round4_etl_statsdf_fewer_events_BL1A.csv




(circuit_scoring) [bcummins@c455-071 ~]$  CONFIGFILE="../circuit_scoring/src/configs/ys_round_trip_rawlog10_drop_nan_bin_BL1A.json" 



(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_raw_log10_stats.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-1/20210921161428/YeastSTATES-1-0-Time-Series-Round-1__fc_meta.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round1_rawlog10_statsdf_drop_nan_BL1A.csv




(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_raw_log10_stats.csv" 

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-3-0/20210923132226/YeastSTATES-1-0-Time-Series-Round-3-0__fc_meta.csv" 

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round3_rawlog10_statsdf_drop_nan_BL1A.csv




(circuit_scoring) [bcummins@c455-071 ~]$  DATAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_raw_log10_stats.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  METAFILE="/work/projects/SD2E-Community/prod/projects/sd2e-project-43/reactor_outputs/preview/YeastSTATES-1-0-Time-Series-Round-4-0/20210920180559/YeastSTATES-1-0-Time-Series-Round-4-0__fc_meta.csv"

(circuit_scoring) [bcummins@c455-071 ~]$  python ../circuit_scoring/call_job.py $DATAFILE $METAFILE $CONFIGFILE YSRoundTrip_with_circuit_scoring_package/BL1_A_analysis/Round4_rawlog10_statsdf_drop_nan_BL1A.csv


```