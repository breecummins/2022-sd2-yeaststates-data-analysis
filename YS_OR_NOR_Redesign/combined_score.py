import pandas as pd
import seaborn as sns
# sns.set(rc={'figure.figsize':(6,4)})
import matplotlib.pyplot as plt
import numpy as np

def get_scores(grouped_df, savename, names_key,thresholds=[0.49999,0.50001]):
    # param thresholds is the proportion thresholds for off and on to have successes
    # savename will have thresholds appended
    final_dict = {"group" : [], "strain" : [], "circuit":[], "plate" : [], 28 : [], 40 : [], 52 : [], "intended_output" : [], "success" : [],  "missing" : [], "dsgrn" : [], "cdm" : []}
    # build df
    for group in grouped_df.groups.keys():
        strain = names_key[group[0]]
        circuit = group[1]
        exp_id = group[3] 
        media = group[4]
        gp = grouped_df.get_group(group)
        p28v = gp.loc[gp["timepoint"] == 28]["pON/(pON+pOFF)"]
        p28 = None if len(p28v) == 0 else p28v.values[0]
        p40v = gp.loc[gp["timepoint"] == 40]["pON/(pON+pOFF)"]
        p40 = None if len(p40v) == 0 else p40v.values[0]
        p52v = gp.loc[gp["timepoint"] == 52]["pON/(pON+pOFF)"]
        p52 = None if len(p52v) == 0 else p52v.values[0]
        time = {28:p28,40:p40,52:p52}
        miss = [x for x in [28,40,52] if time[x] is None]
        if miss:
            final_dict["missing"].append([group,miss])
        else:
            # record vals
            final_dict["group"].append(group)
            final_dict["strain"].append(strain)
            final_dict["circuit"].append(circuit)
            out =int(gp["intended_output"].unique()[0])
            final_dict["intended_output"].append(out)
            final_dict["plate"].append(exp_id.split(".")[-1])
            final_dict[28].append(p28)
            final_dict[40].append(p40)
            final_dict[52].append(p52)

            # identify trends 
            if p28 == p40 and p40==p52:
                trend = "stationary"
            elif p28 <= p40 and p40 <= p52:
                trend = "increasing"
            elif p28 >= p40 and p40 >= p52:
                trend = "decreasing"
            else:
                trend = "indeterminate"

            # make categories for grouping in figures
            if "DSGRN" in strain:
                final_dict["dsgrn"].append("DSGRN {}".format(circuit))
            elif "Simple" in strain:
                final_dict["dsgrn"].append("Simple {}".format(circuit))
            else:
                raise ValueError("Missing DSGRN or Simple in {}".format(group))
            if "low" in strain:
                final_dict["cdm"].append("{} low".format(circuit))
            elif "high" in strain:
                final_dict["cdm"].append("{} high".format(circuit))
            else:
                raise ValueError("Missing low or high in {}".format(group))
            
            correct_off = (out == 0 and (sum([p <=thresholds[0] for p in [p28,p40,p52]]) >= 3))
            correct_on = (out == 1 and (sum([p >= thresholds[1] for p in [p28,p40,p52]]) >= 3))
            trend_correct_off = (out == 0 and trend == "decreasing")
            trend_correct_on = (out == 1 and trend == "increasing")
            if correct_off or correct_on:
                final_dict["success"].append(1)
            elif trend_correct_off or trend_correct_on:
                final_dict["success"].append(1)
            else:
                final_dict["success"].append(0)

    # save to file
    missing = final_dict.pop("missing")
    print("Number missing is {}".format(len(missing)))
    df = pd.DataFrame.from_dict(final_dict)
    print("Number present is {}".format(len(df)))
    savename_start = savename.split(".csv")[0]
    off = int(thresholds[0]*100)
    on = int(thresholds[1]*100)
    df.to_csv(savename_start+"_{}_{}.csv".format(off,on))
    return df,final_dict


def aggregate_scores(df,names_key,exp_ids,savename=None,thresholds=None):
    results_dict = {"strain" : [], "plate" : [], "circuit" : [], "media" : [], "success" : [], "events on plate" :[],"# success":[]}
    predictions_dict = {"DSGRN NOR" : {ei.split(".")[-1] : [] for ei in exp_ids},"DSGRN OR": {ei.split(".")[-1] : [] for ei in exp_ids}, "Simple NOR": {ei.split(".")[-1] : [] for ei in exp_ids},"Simple OR": {ei.split(".")[-1] : [] for ei in exp_ids},"NOR low": {ei.split(".")[-1] : [] for ei in exp_ids},"NOR high": {ei.split(".")[-1] : [] for ei in exp_ids},"OR low": {ei.split(".")[-1] : [] for ei in exp_ids},"OR high": {ei.split(".")[-1] : [] for ei in exp_ids}}

    for strain in names_key:
        sdf = df[df["group"].apply(lambda x: strain in x)]
        for ei in exp_ids:
            sedf = sdf[sdf["group"].apply(lambda x: ei in x)]
            if not sedf.empty:
                ss = names_key[strain]
                ss.replace("Orig","Simple")
                results_dict["strain"].append(ss)
                plate = ei.split(".")[-1]
                results_dict["plate"].append(plate)
                if "Synthetic_Complete" in sedf["group"].unique()[0]:
                    results_dict["media"].append("SC")
                elif "YEP 2%-dextrose" in sedf["group"].unique()[0]:
                    results_dict["media"].append("YEP")
                elif "Synthetic_Complete_1%Sorbitol" in sedf["group"].unique()[0]:
                    results_dict["media"].append("SB")
                else:
                    raise ValueError("Media {} not recognized.".format(sedf["group"].unique()[0]))
                if "NOR" in sedf["group"].unique()[0]:
                    results_dict["circuit"].append("NOR")
                else:
                    results_dict["circuit"].append("OR")
                number_successes = sedf["success"].sum()
                # There is a choice of how to score. One can score by dividing everything by 
                # the number of samples on the plate, or by the number of samples that remained after filtering.
                # In dividing by 10, there is an assumption that the circuit had something to do with the failure
                # of the sample, which may not be true, but is conservative.
                total_samples_on_plate_per_strain = 10
                number_samples_after_filtering = len(sedf)
                results_dict["events on plate"].append(number_samples_after_filtering)
                results_dict["# success"].append(number_successes)
                results_dict["success"].append(number_successes / total_samples_on_plate_per_strain)
                # results_dict["success vectors"].append(sedf["success"])
                # tot_group_samples = 20
                design,circuit,cdm = names_key[strain].split()
                if design=="Orig":
                    design=="Simple"
                dsgrn = " ".join([design,circuit])
                cdm = " ".join([circuit,cdm])
                predictions_dict[dsgrn][plate].extend(sedf["success"])
                predictions_dict[cdm][plate].extend(sedf["success"])
    pred_results = {"dsgrn":[],"dsgrn success":[],"cdm":[],"cdm success":[],"plate":[]}
    for pred,plates in predictions_dict.items():
        for plate,succ_vec in plates.items():
            if pred.startswith("DSGRN") or pred.startswith("Simple"):
                pred_results["dsgrn"].append(pred)
                pred_results["dsgrn success"].append(sum(succ_vec)/20)                
                pred_results["plate"].append(plate)
                pred_results["cdm"].append(None)
                pred_results["cdm success"].append(None)
            elif pred.startswith("OR") or pred.startswith("NOR"):
                pred_results["cdm"].append(pred)
                pred_results["cdm success"].append(sum(succ_vec)/20)
                pred_results["plate"].append(plate)
                pred_results["dsgrn"].append(None)
                pred_results["dsgrn success"].append(None)                
            else:
                raise ValueError("Wrong key. Shouldn't get here.")
    # for key,val in pred_results.items():
    #     print("{}: {}".format(key,len(val)))
    predictions_df = pd.DataFrame(pred_results)
    results_df = pd.DataFrame(results_dict)
    if savename and thresholds:
        savename_start = savename.split(".csv")[0]
        off = int(thresholds[0]*100)
        on = int(thresholds[1]*100)
        results_df.to_csv(savename_start+"_{}_{}_aggregated_results.csv".format(off,on))
        predictions_df.to_csv(savename_start+"_{}_{}_aggregated_predictions.csv".format(off,on))
    print("Filtered samples per plate (max 10*15 or 10*14 for dropped wsh), then avg prop successes of 10.")
    stats={}
    for _,name in names_key.items():
        perplate=list(results_df.loc[results_df["strain"] == name]["events on plate"].values)
        prop_perplate=list(results_df.loc[results_df["strain"] == name]["success"].values)
        diffquar=np.percentile(prop_perplate,75)-np.percentile(prop_perplate,25)
        median=np.percentile(prop_perplate,50)
        mean=np.mean(prop_perplate)
        stats[name]=[mean,median,diffquar]
        print(name)
        print("sum of samples: {}, avg success: {:.2f}".format(sum(perplate),sum(prop_perplate)/len(prop_perplate)))
    return results_df,stats,predictions_df


def plot_by_plate_category_media(results_df,plates,savefigname,value_vars,ylim=[-0.1,1.1]):
    # sns.set(font_scale=2)
    plate_df = results_df.loc[results_df["plate"].isin(plates)]
    plate_df = plate_df.reset_index()
    plate_df_melt = plate_df.melt(id_vars=['index','strain','media'], value_vars=value_vars, value_name='Proportion of success')
    b = sns.boxplot(data=plate_df_melt, x='strain', y='Proportion of success', hue='media', order=["DSGRN NOR low", "DSGRN NOR high", "Simple NOR low", "Simple NOR high","DSGRN OR low","DSGRN OR high","Simple OR low","Simple OR high"], hue_order=["SB","YEP","SC"],showmeans=True,
            meanprops={"marker":"o",
                       "markerfacecolor":"black", 
                       "markeredgecolor":"black",
                      "markersize":"6"})
    b.set_ylabel('Proportion of success',fontsize=16)
    b.tick_params(labelsize=14)
    plt.xlabel('')
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.85),fontsize=14)
    plt.tight_layout()
    plt.xticks(rotation=40,ha='right')
    plt.ylim(ylim)
    # plt.rcParams.update({'font.size': 22})
    # sns.swarmplot(data=plate_df_melt, x='strain', y='Proportion of success', order=["DSGRN NOR low", "DSGRN NOR high", "Orig NOR low", "Orig NOR high","DSGRN OR low","DSGRN OR high","Orig OR low","Orig OR high"])
    # plt.gcf().set_size_inches(4,3)
    plt.savefig(savefigname, dpi=300, format='png',bbox_inches="tight")    
    plt.show()


def plot_by_plate_category(results_df,plates,savefigname,value_vars,ylim=[-0.1,1.1]):
    # sns.set(font_scale=2)
    plate_df = results_df.loc[results_df["plate"].isin(plates)]
    plate_df = plate_df.reset_index()
    plate_df_melt = plate_df.melt(id_vars=['index','strain','circuit'], var_name='success',value_vars=value_vars, value_name='Proportion of success')
    print(len(plate_df.loc[plate_df["plate"] == plates[0]]))
    print(len(plate_df_melt))
    b = sns.boxplot(data=plate_df_melt, x='strain', y='Proportion of success', hue='circuit', order=["DSGRN NOR low", "DSGRN NOR high", "Simple NOR low", "Simple NOR high","DSGRN OR low","DSGRN OR high","Simple OR low","Simple OR high"],hue_order=["NOR","OR"],showmeans=True,
            meanprops={"marker":"o",
                       "markerfacecolor":"black", 
                       "markeredgecolor":"black",
                      "markersize":"6"})
    b.set_ylabel('Proportion of success',fontsize=16)
    b.tick_params(labelsize=14)
    plt.xticks(rotation=40,ha='right')
    plt.ylim(ylim)
    plt.xlabel('')
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.85),fontsize=14)

    plt.tight_layout()
    # plt.rcParams.update({'font.size': 22})
    # sns.swarmplot(data=plate_df_melt, x='strain', y='Proportion of success', order=["DSGRN NOR low", "DSGRN NOR high", "Orig NOR low", "Orig NOR high","DSGRN OR low","DSGRN OR high","Orig OR low","Orig OR high"])
    # This set size does not change the size of the saved figure, even though it displays correctly. I do not know why.
    plt.gcf().set_size_inches(7,5.25,forward=True)
    plt.savefig(savefigname,bbox_inches="tight")    
    plt.show()


def plot_by_plate_category_predictions(results_df,plates,savefigname,ylim=[-0.1,1.1]):
    plate_df = results_df.loc[results_df["plate"].isin(plates)]
    plate_df = plate_df.reset_index()

    plate_df_melt = plate_df.melt(id_vars=['index','dsgrn'], var_name='Redesign',value_vars=['dsgrn success'], value_name='Proportion of success')
    b = sns.boxplot(data=plate_df_melt, x='dsgrn', y='Proportion of success', hue='Redesign', order=["DSGRN NOR", "Simple NOR","DSGRN OR","Simple OR"],showmeans=True,
            meanprops={"marker":"o",
                       "markerfacecolor":"black", 
                       "markeredgecolor":"black",
                      "markersize":"6"})
    b.set_ylabel('Proportion of success',fontsize=16)
    b.tick_params(labelsize=14)
    plt.xticks(rotation=40,ha='right')
    plt.xlabel('')
    plt.ylim(ylim)
    # plt.xlabel("Redesign")
    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.85),fontsize=14)
    plt.legend([],[], frameon=False)

    plt.tight_layout()
    # plt.rcParams.update({'font.size': 22})
    # sns.swarmplot(data=plate_df_melt, x='dsgrn', y='Proportion of success', order=["DSGRN NOR", "Orig NOR","DSGRN OR","Orig OR"])
    # plt.gcf().set_size_inches(4,3)
    plt.savefig(savefigname+"_dsgrn.pdf", bbox_inches="tight")    
    plt.show()

    plate_df_melt = plate_df.melt(id_vars=['index','cdm'], var_name='CDM parts',value_vars=['cdm success'], value_name='Proportion of success')
    b = sns.boxplot(data=plate_df_melt, x='cdm', y='Proportion of success', hue='CDM parts', order=["NOR low", "NOR high","OR low","OR high"],showmeans=True,
            meanprops={"marker":"o",
                       "markerfacecolor":"black", 
                       "markeredgecolor":"black",
                      "markersize":"6"})
    b.set_ylabel('Proportion of success',fontsize=16)
    b.tick_params(labelsize=14)
    plt.xticks(rotation=40,ha='right')
    plt.xlabel('')
    plt.ylim(ylim)
    # plt.xlabel("CDM parts")
    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.85),fontsize=14)
    plt.legend([],[], frameon=False)
    plt.tight_layout()
    # plt.rcParams.update({'font.size': 22})
    # sns.swarmplot(data=plate_df_melt, x='cdm', y='Proportion of success', order=["NOR low", "NOR high","OR low","OR high"])
    # plt.gcf().set_size_inches(4,3)
    plt.savefig(savefigname+"_cdm.pdf", bbox_inches="tight")    
    plt.show()



