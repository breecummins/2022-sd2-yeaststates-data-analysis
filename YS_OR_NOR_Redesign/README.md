These are summary instructions for data analysis on the Titration and Fixed Ratio experiments.

# PREPROCESSING FROM SIGNAL PREDICTION FILES

Compute predicted ON/OFF ratio and filter datasets by ratio. 


```bash

$  python preprocess_data.py ratio data/titration_fcs_signal_prediction data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Titration/20210723174537 

$  python preprocess_data.py ratio data/fixed_ratio_fcs_signal_prediction data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Fixed-Ratio/20210909142014/

```

Concatenate signal prediction files. Titration BEP model HL1 has CEN.PK as the positive control. Fixed Ratio BEP model HL2 has CEN.PK as the positive control.

```bash
$  python preprocess_data.py concat data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Titration/20210723174537/ Titration HL1 cleanModel

$  python preprocess_data.py concat data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Fixed-Ratio/20210909142014/ FixedRatio HL2 cleanModel

```

In dual inducer experiments, the concentrations of BE and Dox have to be split out from strings because of metadata construction.

```bash

$  python preprocess_data.py inducer data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Titration/20210723174537/Titration_HL1_cleanModel_meta_concat.csv

$  python preprocess_data.py inducer data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Fixed-Ratio/20210909142014/FixedRatio_HL2_cleanModel_meta_concat.csv

```

The following is for dropping the positive control that is not associated to the ML model (we want CEN.PK as the positive control, not UWBF_6390).

```bash

$  python preprocess_data.py control data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Titration/20210723174537/Titration_HL1_cleanModel_meta_concat_split_inducers.csv data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Titration/20210723174537/Titration_HL1_cleanModel_raw_log10_concat.csv UWBF_6390

$  python preprocess_data.py control data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Fixed-Ratio/20210909142014/FixedRatio_HL2_cleanModel_meta_concat_split_inducers.csv data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Fixed-Ratio/20210909142014/FixedRatio_HL2_cleanModel_raw_log10_concat.csv UWBF_6390

```

The following polishes the metadata file in anticipation of time trend analysis of the fluorescence signal.

```bash

$  python preprocess_data.py timetrend data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Titration/20210723174537/Titration_HL1_cleanModel_meta_concat_split_inducers_drop_UWBF_6390.csv data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Titration/20210723174537/Titration_HL1_cleanModel_raw_log10_concat_drop_UWBF_6390.csv 

$  python preprocess_data.py timetrend data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Fixed-Ratio/20210909142014/FixedRatio_HL2_cleanModel_meta_concat_split_inducers_drop_UWBF_6390.csv data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Fixed-Ratio/20210909142014/FixedRatio_HL2_cleanModel_raw_log10_concat_drop_UWBF_6390.csv 

```

Plate `wsh` in Fixed Ratio had wide-spread poor growth conditions and was dropped from final analysis.

```bash
$   python preprocess_data.py dropplate data/filtered_signal_prediction/YeastSTATES-Dual-Response-CRISPR-Redesigns-Short-Duration-Time-Series-30C-Fixed-Ratio/20210909142014/FixedRatio_HL2_cleanModel_meta_concat_split_inducers_drop_UWBF_6390_timetrend.csv 

```

# SCORING

The final files were moved to `processed_metadata_for_time_trending_2021_12_13`. Now open and run the Jupyter notebook `combined_score_analysis.ipynb`.


