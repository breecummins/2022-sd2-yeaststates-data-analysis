import pandas as pd
import numpy as np
import scoring_dependencies as sd
import combined_score as cs
import multiprocessing as mp
import functools


names_key = {"UWBF_36564" : "DSGRN NOR low", "UWBF_36565" : "DSGRN NOR high", "UWBF_36566" : "DSGRN OR low", "UWBF_36567"  : "DSGRN OR high", "UWBF_36568" : "Simple NOR low", "UWBF_36569" : "Simple NOR high", "UWBF_36570"  : "Simple OR low", "UWBF_36571"  : "Simple OR high"}

good_plates = ["r1fw4srz5dcaew","r1fw4sqpyb7sww","r1fqsfs6pm98h9", "r1fqsfkwxcccv6", "r1fqsftgsftdtw", "r1fqsfn8au7bvp", "r1fqsfutgc5bab", "r1fqsfyqupyc24", "r1fqsfxekah8dh"]

bad_plates = ["r1fw4stb38ewsh", "r1fqsg227dzvkj","r1fqsfw543tey6", "r1fqsfpj7u6prt", "r1fqsfqupgtnkv", "r1fqsg3bptd4mj"]


def scramble(df,permute):
    plates = df["experiment_id"].unique()
    for plate in plates:
        if permute:
            # The following line permutes the data values
            random_vals = np.random.permutation(df[df["experiment_id"] == plate]["pON/(pON+pOFF)"].values)
        else:
            # The following line inserts random values between 0 and 1
            random_vals = np.random.random(len(df[df["experiment_id"]==plate]))
        df.loc[df["experiment_id"] == plate,"pON/(pON+pOFF)"] = random_vals
    return df


def compute_scores(meta_df,thresholds):
    grouped_df,exp_ids = sd.analyze(meta_df)
    df,_ = cs.get_scores(grouped_df,"temp.csv",names_key,thresholds)
    results_df,_,_ = cs.aggregate_scores(df,names_key,exp_ids)
    return results_df

def stack(meta_df,thresholds,permute,N):
    print("Iteration {}".format(N))
    df = scramble(meta_df.copy(),permute)
    results_df = compute_scores(df,thresholds)
    return results_df


def compute_baseline(meta_fr,meta_ti,num_iter,thresholds,savefigname,permute=True):
    fr_df = pd.read_csv(meta_fr)
    ti_df = pd.read_csv(meta_ti)
    meta_df = pd.concat([fr_df,ti_df],ignore_index=True)
    meta_df = meta_df.loc[meta_df["strain"].isin(names_key.keys())]
    meta_df.drop(["pON","pOFF","pON/pOFF"], axis=1)
    pool = mp.Pool()
    baseline = functools.partial(stack,meta_df,thresholds,permute)
    output = list(pool.map(baseline,range(num_iter)))
    baseline_df = pd.concat(output)
    baseline_df.to_csv(savefigname.split()[0]+"_scores.csv")
    cs.plot_by_plate_category(baseline_df,good_plates+bad_plates,savefigname,['success'])
    return baseline_df


def plot_only(fname,savefigname):
    df = pd.read_csv(fname)
    df['strain'] = df['strain'].str.replace('Orig','Simple')
    cs.plot_by_plate_category(df,good_plates+bad_plates,savefigname,['success'])



if __name__ == "__main__":
    
    # meta_fr = "processed_metadata_for_time_trending_2021_12_13/FixedRatio_HL2_cleanModel_meta_concat_split_inducers_drop_UWBF_6390_timetrend_drop_plate_wsh.csv"
    # meta_ti = "processed_metadata_for_time_trending_2021_12_13/Titration_HL1_cleanModel_meta_concat_split_inducers_drop_UWBF_6390_timetrend.csv"
    # savefigname = "cleanModel_baseline.pdf"
    # numiter = 1000
    # thresholds = [0.4,0.6]
    # compute_baseline(meta_fr,meta_ti,numiter,thresholds,savefigname,permute=True)

    plot_only("results_2022_05_06/cleanModel_baseline_scores.csv","cleanModel_baseline.pdf")






    
    