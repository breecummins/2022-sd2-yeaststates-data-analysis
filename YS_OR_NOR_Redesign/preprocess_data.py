import pandas as pd
import sys,os
import numpy as np


def cat_signal_prediction_files(input_folder,savetag,identifier,model):
    # savetag is either Titration or FixedRatio
    # identifier is either HL1 or HL2
    # model is either fullModel or cleanModel
    meta_df = pd.DataFrame()
    data_log_df = pd.DataFrame()
    data_etl_df = pd.DataFrame()
    dir_name = os.path.abspath(os.path.expanduser(input_folder))

    def mergeme(persist_df,df):
        if not persist_df.empty:
            perdf = pd.concat([persist_df,df],ignore_index=True)
        else:
            perdf = df.copy()
        return perdf

    count_meta = 0
    count_data = 0
    for fname in os.listdir(dir_name):
        if "pdt" in fname and model in fname and identifier in fname and fname.endswith(".csv"):
            print(fname)
            df = pd.read_csv(os.path.join(dir_name,fname))
            if "meta" in fname:
                meta_df = mergeme(meta_df,df)
                count_meta += df.shape[0]
            elif "log10" in fname:
                data_log_df = mergeme(data_log_df,df)
                count_data += df.shape[0]
            elif "etl" in fname:
                data_etl_df = mergeme(data_etl_df,df)
            else:
                pass
    #test merge
    assert(meta_df.shape[0] == count_meta)
    assert(data_log_df.shape[0] == count_data)
    # The following assert statement is not guaranteed to be true. There will generally be more samples (each sample_id has many channels) but not an even multiple because samples can be missing channel data.
    # assert(meta_df.shape[0] == data_log_df.shape[0])
    if meta_df.empty:
        raise ValueError("No metadata in folder!")
    if data_log_df.empty and data_etl_df.empty:
        raise ValueError("No data in folder!")
    #save
    def saveme(df,name):
        if not df.empty:
            savefile = os.path.join(input_folder,"{}_{}_{}_{}_concat.csv".format(savetag,identifier,model,name))
            print(savefile)
            df.to_csv(savefile,index=False)

    saveme(meta_df,"meta")
    saveme(data_log_df,"raw_log10")
    saveme(data_etl_df,"etl")



def calc_all_ratios(dir_name,savedir):
    dir_name = os.path.abspath(os.path.expanduser(dir_name))
    for fname in os.listdir(dir_name):
        if "pdt" in fname and "raw_log10" not in fname and "etl" not in fname and "cleanControls" not in fname:
            data_head = os.path.basename(fname).split("signal_prediction")[0]
            fname = os.path.join(dir_name,fname)
            meta_fname = calculate_pON_over_pOFF(fname,savedir)
            print(meta_fname)
            for dname in os.listdir(dir_name):
                if dname.startswith(data_head) and ("raw_log10" in dname or "etl" in dname):
                    dname = os.path.join(dir_name,dname)
                    data_fname = filter_by_pRatio(meta_fname,dname)
                    print(data_fname)
            print("\n")


def calculate_pON_over_pOFF(meta_fname,savedir):
    df = pd.read_csv(meta_fname)
    # since the following is a numpy operation, if df["pOFF"] == 0, then the resulting division returns np.inf. 
    df["pON/pOFF"] = df["pON"].values / df["pOFF"].values
    # replace inf by the ON sample number so that it will be interpreted as a number upon loading the df
    df["pON/pOFF"] = np.where(df["pOFF"] == 0, df["pON"], df["pON/pOFF"])
    fname = os.path.basename(meta_fname).split(".")[0].split("__fc_meta")[0] + "__fc_meta__pRatio.csv"
    path2meta = os.path.join(savedir,fname)
    df.to_csv(path2meta,index=False)
    return path2meta


def filter_by_pRatio(pratio_meta_fname,data_file):
    meta_df = pd.read_csv(pratio_meta_fname)
    data_df = pd.read_csv(data_file)
    meta_df = meta_df.drop(meta_df.columns.difference(["sample_id","pON/pOFF"]), axis=1, inplace=False)
    data_df = data_df.merge(meta_df,on="sample_id",how="inner")
    # pON/pOFF ratios that are exactly 1 are dropped. This should happen only very rarely.
    # "predicted_output" is the result of the ML method for separating histograms, NOT the truth table value. There is a value of 0 and of 1 for each histogram. The following line chooses either 0 or 1 depending on the pRatio. This halves the size of the files.
    data_df = data_df.loc[((data_df["pON/pOFF"]>1) & (data_df["predicted_output"] == 1)) | ((data_df["pON/pOFF"]<1) & (data_df["predicted_output"] == 0))]
    fname = os.path.basename(data_file).split(".")[0] + "__pRatio.csv"
    savedir = os.path.dirname(pratio_meta_fname)
    path2data = os.path.join(savedir,fname)
    data_df.to_csv(path2data,index=False)
    return path2data
    

def redesign_split_inducers(meta_fname,savename=None):
    df = pd.read_csv(meta_fname,dtype=object)
    inducer_beta = []
    inducer_dox = []
    for i,v in zip(df["inducer_type"].values,df["inducer_concentration"].values):
        if "|" in v:
            vb, vd = v.split("|")
        elif i.startswith("b"):
            vb, vd = v,0
        elif i.startswith("d"):
            vb, vd = 0,v
        inducer_beta.append(vb)
        inducer_dox.append(vd)
    df["beta-estradiol"] = inducer_beta
    df["doxycycline-hyclate"] = inducer_dox
    if not savename:
        savename = os.path.join(os.path.dirname(meta_fname),os.path.basename(meta_fname).split(".")[0]+"_split_inducers.csv")
    df.to_csv(savename)
    return df, savename


def pos_control_drop(meta_fname,data_fname,drop_strain):
    meta_df = pd.read_csv(meta_fname)
    data_df = pd.read_csv(data_fname)
    samp_ids = meta_df.loc[meta_df["strain"] == drop_strain]["sample_id"].to_list()
    meta_df = meta_df.drop(meta_df[meta_df["sample_id"].isin( samp_ids)].index,inplace=False)
    data_df = data_df.drop(data_df[data_df["sample_id"].isin( samp_ids)].index,inplace=False)
    savemeta = os.path.join(os.path.dirname(meta_fname),os.path.basename(meta_fname).split(".")[0]+"_drop_{}.csv".format(drop_strain))
    savedata = os.path.join(os.path.dirname(data_fname),os.path.basename(data_fname).split(".")[0]+"_drop_{}.csv".format(drop_strain))
    meta_df.to_csv(savemeta)
    data_df.to_csv(savedata)


def time_trend(meta_fname,data_fname):
    meta_df = pd.read_csv(meta_fname)
    data_df = pd.read_csv(data_fname)
    data_df = data_df.loc[data_df["channel"]=="BL1-H"]
    data_df = data_df.drop(data_df.columns.difference(["sample_id","cells/mL","total_counts"]), axis=1, inplace=False)
    # merge cells/mL and total counts from data file into meta file for filtering
    df = data_df.merge(meta_df,on="sample_id",how="inner")
    # make sure samples are performing well
    df = df.loc[df["cells/mL"].ge(500000)]
    df = df.loc[df["total_counts"].ge(10000)]
    # add normalized version of ON-OFF ratio
    df["pON/(pON+pOFF)"] = df["pON"].values / ( df["pON"].values + df["pOFF"].values)
    # make uniform the inducer concentrations
    df["beta-estradiol"] = pd.to_numeric(df["beta-estradiol"])
    df["beta-estradiol"] = np.where(df["beta-estradiol"] < 1, df["beta-estradiol"]*1e6, df["beta-estradiol"])
    df["doxycycline-hyclate"] = pd.to_numeric(df["doxycycline-hyclate"])
    df["doxycycline-hyclate"] = np.where(df["doxycycline-hyclate"] < 1, df["doxycycline-hyclate"]*1e6, df["doxycycline-hyclate"])
    savemeta = os.path.join(os.path.dirname(meta_fname),os.path.basename(meta_fname).split(".")[0]+"_timetrend.csv")
    df.to_csv(savemeta)


def exclude_plate_wsh(meta_fname):
    meta_df = pd.read_csv(meta_fname)
    df = meta_df[~meta_df["experiment_id"].str.contains("r1fw4stb38ewsh")]
    savemeta = os.path.join(os.path.dirname(meta_fname),os.path.basename(meta_fname).split(".")[0]+"_drop_plate_wsh.csv")
    df.to_csv(savemeta)

    


def test():
    cat_signal_prediction_files("~/Desktop/temp2","Titration","HL1")


if __name__=="__main__":

    # test()

    # arg is which function to call: ratio, concat, inducer, control, timetrend
    func_type = sys.argv[1]

    if func_type == "ratio":
        # script for calculating predicted ON/OFF ratio for each bimodal histogram
        # first arg is input dir, second arg is output dir, third arg is HL1 or HL2 (HL1 has Cen.PK controls)
        calc_all_ratios(sys.argv[2],sys.argv[3])
    elif func_type == "concat":
        # script concatenating files from signal prediction altogether
        # args: input_folder (location of files to concat),savetag (Titration or FixedRatio), identifier (HL1 or HL2; HL1 has Cen.PK controls), model (fullModel or cleanModel, full model uses original controls to separate bimodality and clean model uses ML cleaned controls to do so)
        cat_signal_prediction_files(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
    elif func_type == "inducer":
        # script for preprocessing redesign data by making individual columns for inducers
        # args: metadata file, name under which to save new meta file (default is meta file name with '_split_inducers' appended stored in the same directory)
        if len(sys.argv) < 4:
            df, savename = redesign_split_inducers(sys.argv[2])
        else:
            df, savename = redesign_split_inducers(sys.argv[2],sys.argv[3])
    elif func_type == "control":
        # script for dropping one of the positive controls based on the ML model. For HL1 this is UWBF_6390 and for HL2 this is UWBF_24864
        # args: meta data file name, data file name, and the strain to drop
        pos_control_drop(sys.argv[2],sys.argv[3],sys.argv[4])
    elif func_type == "timetrend":
        # script for computing normalized on/off ratios and for putting inducer concs all in the same units
        time_trend(sys.argv[2],sys.argv[3])
    elif func_type == "dropplate":
        # script for dropping plate r1fw4stb38ewsh due to low cell density at time points 40 and 52
        exclude_plate_wsh(sys.argv[2])



