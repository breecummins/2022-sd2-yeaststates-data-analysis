import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def analyze(meta_df,beta_inducer=None,dox_inducer=None):
    meta_df = meta_df.loc[meta_df["strain"].isin(["UWBF_36564","UWBF_36565","UWBF_36566","UWBF_36567","UWBF_36568","UWBF_36569","UWBF_36570","UWBF_36571"])]
    keep_cols=["strain","strain_circuit","experiment_id","media_type","beta-estradiol","doxycycline-hyclate","plate", "strain_input_state","replicate","timepoint","pON/(pON+pOFF)","intended_output"]
    trunc_df = meta_df.drop(meta_df.columns.difference(keep_cols), axis=1, inplace=False)
    if beta_inducer:
        trunc_df = trunc_df.loc[trunc_df["beta-estradiol"].isin(beta_inducer)]
    if dox_inducer:
        trunc_df = trunc_df.loc[trunc_df["doxycycline-hyclate"].isin(dox_inducer)]
    group_cols = ["strain","strain_circuit","strain_input_state","experiment_id","media_type","beta-estradiol","doxycycline-hyclate","replicate"]
    exp_ids = list(meta_df["experiment_id"].unique())
    grouped_df = trunc_df.groupby(group_cols)
    print("Size concat: {}, Size grouped: {}".format(len(meta_df),len(grouped_df)))
    return grouped_df,exp_ids


