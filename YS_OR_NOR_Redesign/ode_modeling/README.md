# Gate Characterization using lmfit

This code is associated with the pre-print manuscript "A feature-rich DBTL loop for designing synthetic logic circuits for robust and reproducible performance".



### files

`BetareresponsesINPUT.xlsx`

Beta inducible promoter characterization data

`DoxareresponsesINPUT.xlsx`

DOX inducible promoter characterization data

`Dosreresponses2Beta.xlsx`

Beta repressible gate characterization

`Dosreresponses2Dox.xlsx`

DOX repressible gate characterization

`UsingLMFIT.py`

Python executable file script used to fit gate dynamics to the Hill equation

`requirements.txt`

Python external packages needed to run the python script
