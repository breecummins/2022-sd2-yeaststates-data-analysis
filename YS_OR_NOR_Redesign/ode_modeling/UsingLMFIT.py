# Importing modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from lmfit import minimize, Parameters, Parameter, report_fit
from pathlib import Path


# Define model function (g), and residual function to be minimized (residual)
def g(paras, t):
    """
    Hill functions used to fit the data to the model
    """  
    try:
        ymax = paras['ymax'].value
        ymin = paras['ymin'].value
        K = paras['K'].value
        n = paras['n'].value

    except KeyError:
        ymax, ymin, K, n = paras
    
    ### Repression model: for characterizing repression gates
    hill_numbers = ymin + (ymax - ymin)*(K**n)/((t**n) + (K**n))

    ### Activation model: for characterizing activation (input) gates
    #hill_numbers = ymin + (ymax - ymin)*(t**n)/((t**n) + (K**n))

    return hill_numbers

def residual(paras, t, data):
    """
    Compute the residual between actual data and the fitted data
    """    
    model = g(paras, t)

    return (model - data).ravel()

#### Importing measured data
p = Path('.')
### For DOX and BE response curves, please uncomment line 24, and comment out line 27
## Uncomment next line for DOX response curves
df = pd.read_excel (p.absolute() / 'Dosreresponses2Dox.xlsx')
# Uncomment next line for BE response curves
#df = pd.read_excel (p.absolute() / 'Dosreresponses2BE.xlsx')

### For DOX and BE input gate response curves, please uncomment line 28, and comment out line 25
# Uncomment next line for DOX input promoter parameterization (activation)
#df = pd.read_excel (p.absolute() / 'DoxreresponsesINPUT.xlsx')
# Uncomment next line for BE input promoter parameterization (activation)  
#df = pd.read_excel (p.absolute() / 'BetareresponsesINPUT.xlsx')      


#### Sort inducer concentration
inputs = df.Inducer
inputs = inputs.tolist()
inputs = np.asarray(inputs)

names = []
outputs = []
for column in df.columns[1:]:
    names.append(column.split())
    outputs.append(list(df[column]))

# Create dataframe to store all the characterizations
fitted_results = pd.DataFrame(index=['ymax','ymin','K','n'])

# Loop through all the data columns
for k in range(len(outputs)):
    print(names[k])
    y_measured = outputs[k]

    # Create figure for measured (experimental) data
    fig = plt.figure()
    ax = plt.gca()
    plt.scatter(inputs, y_measured, marker='o', color='b', label='measured data', s=75)
    ax.set_xscale("log")
    ax.set_yscale("log")


    # Set parameters including bounds; you can also fix parameters (use vary=False)
    params = Parameters()
    ### Make sure you have good initial value estimates, otherwise fitting is not going to work
    params.add('ymax', value=max(y_measured), vary=False) #Maybe set this parameter as fixed?
    params.add('ymin', value=min(y_measured), vary=False)
    params.add('K', value=4000, min=0.0001, max=10000.)
    params.add('n', value=4, min=0.0001, max=10000.)

    # Fit model
    result = minimize(residual, params, args=(inputs, y_measured), method='nelder',nan_policy='raise')  # leastsq nelder

    # Display fitted statistics
    report_fit(result)

    # Check results of the fit
    data_fitted = g(result.params,inputs)
    # plot fitted data
    plt.plot(inputs, data_fitted, '-', linewidth=2, color='green', label='fitted data')
    plt.legend()
    plt.show()

    # Add fitted values of gate to dataframe
    fitted_results[names[k][0]] = [result.params['ymax'].value, result.params['ymin'].value, result.params['K'].value, result.params['n'].value]

    # display fitted statistics
    report_fit(result)

# Print results of fitting information
print(fitted_results)


